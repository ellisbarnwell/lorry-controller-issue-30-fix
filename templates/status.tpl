<!DOCTYPE HTML>
<html>
    <head>
        <title>Lorry Controller status</title>
        <link rel="stylesheet" href="/lc-static/style.css" type="text/css" />
    </head>
    <body>
        % import json

        <p>{{warning_msg}}</p>

        <h1>Status of Lorry Controller</h1>

% if links:
%   if running_queue:
        <form method="POST" action="/1.0/stop-queue">
          <p>New jobs are allowed.
            <input type="submit" name="submit" value="Don't allow new jobs" />
            <input type="hidden" name="redirect" value="/1.0/status-html" />
          </p>
        </form>
%   else:
        <form method="POST" action="/1.0/start-queue">
          <p>New jobs are NOT allowed.
            <input type="submit" name="submit" value="Allow new jobs" />
            <input type="hidden" name="redirect" value="/1.0/status-html" />
          </p>
        </form>
%   end
% else:
%   if running_queue:
        <p>New jobs are allowed.</p>
%   else:
        <p>New jobs are NOT allowed.</p>
%   end
% end

% if links:
    <form method="POST" action="/1.0/read-configuration">
      <p>
        <input type="submit" name="submit" value="Re-read configuration" />
        <input type="hidden" name="redirect" value="/1.0/status-html" />
      </p>
    </form>
% end

<p>Maximum number of jobs: {{max_jobs}}.</p>

        <p>Free disk space:
% if disk_free_gib:
             {{disk_free_gib}} GiB.
% else:
             {{disk_free_mib}} MiB.
% end
        </p>

<h2>Upstream Hosts</h2>

<table>
<tr>
<th>Host</th>
<th>Due for re-scan of remote repositories</th>
</tr>
% for host_info in hosts:
<tr>
<td>{{host_info['host']}}</td>
<td>{{host_info['ls_due_nice']}}</td>
</tr>
% end
</table>

        <h2>Currently running jobs</h2>

% if len(run_queue) == 0:
<p>There are no jobs running at this time.</p>
% else:
<table>
<tr>
<th>Job ID</th>
<th>path</th>
</tr>
%     for spec in run_queue:
%         if spec['running_job'] is not None:
%             if links:
<tr>
<td><a href="/1.0/job-html/{{spec['running_job']}}">{{spec['running_job']}}</a></td>
<td><a href="/1.0/lorry-html/{{spec['path']}}">{{spec['path']}}</a></td>
</tr>
%             else:
<tr>
<td>{{spec['running_job']}}</td>
<td>{{spec['path']}}</td>
</tr>
%             end
%         end
%     end
</table>
% end

% if links:
<p>See separate list of <a href="/1.0/list-jobs-html">all jobs that
    have ever been started</a>.</p>

<p>See the list of <a href="/1.0/failures-html">failing lorries</a>.</p>
% end

        <h2>Run-queue</h2>

<table>
<tr>
<th>Pos</th>
<th>Path</th>
<th>Interval</th>
<th>Due</th>
<th>Last run exit</th>
<th>Job?</th>

</tr>
% for i, spec in enumerate(run_queue):
%   obj = json.loads(spec['text'])
%   name = list(obj)[0]
%   fields = obj[name]
<tr>
<td>{{i+1}}</td>
%   if links:
<td><a href="/1.0/lorry-html/{{spec['path']}}">{{spec['path']}}</a></td>
%   else:
<td>{{spec['path']}}</td>
%   end
<td>{{spec['interval_nice']}}</td>
<td nowrap>{{spec['due_nice']}}</td>
%   if publish_failures and spec['last_run_exit'] is not None and spec['last_run_error'] != "":
<td><details>
  <summary>{{spec['last_run_exit']}}: Show log</summary>
  <p><pre>{{spec['last_run_error']}}</pre></p>
</details></td>
%   else:
<td>{{spec['last_run_exit']}}</td>
%   end
%   if spec['running_job'] and links:
<td><a href="/1.0/job-html/{{spec['running_job']}}">{{spec['running_job']}}</a></td>
%   else:
<td></td>
%   end
</tr>
% end
</table>

        <hr />

        <p>Updated: {{timestamp}}</p>

    </body>
</html>
