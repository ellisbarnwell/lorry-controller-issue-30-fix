# Copyright (C) 2014-2019  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import json
import logging
import time
import urllib.parse

import bottle

import lorrycontroller


class ShowLorryBase(object):

    def get_lorry_info_with_job_lists(self, statedb, path):
        obj= statedb.get_lorry_info(path)
        obj['jobs'] = statedb.get_jobs_for_lorry(path)
        obj['failed_jobs'] = statedb.get_failed_jobs_for_lorry(path)
        return obj


class ShowLorry(ShowLorryBase, lorrycontroller.LorryControllerRoute):

    http_method = 'GET'
    path = '/1.0/lorry/<path:path>'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        with self.open_statedb() as statedb:
            try:
                return self.get_lorry_info_with_job_lists(statedb, kwargs['path'])
            except lorrycontroller.LorryNotFoundError as e:
                bottle.abort(404, str(e))


class ShowLorryHTML(ShowLorryBase, lorrycontroller.LorryControllerRoute):

    http_method = 'GET'
    path = '/1.0/lorry-html/<path:path>'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)

        with self.open_statedb() as statedb:
            try:
                lorry_info = self.get_lorry_info_with_job_lists(
                    statedb, kwargs['path'])
            except lorrycontroller.LorryNotFoundError as e:
                bottle.abort(404, str(e))

        renderer = lorrycontroller.StatusRenderer()
        shower = lorrycontroller.JobShower()

        lorry_obj = list(json.loads(lorry_info['text']).values())[0]
        lorry_info['url'] = lorry_obj['url']

        lorry_info['interval_nice'] = renderer.format_secs_nicely(
            lorry_info['interval'])

        lorry_info['last_run_nice'] = time.strftime(
            '%Y-%m-%d %H:%M:%S UTC',
            time.gmtime(lorry_info['last_run']))

        lorry_info['disk_usage_nice'] = shower.format_bytesize(
            lorry_info['disk_usage'])

        now = statedb.get_current_time()

        due = lorry_info['last_run'] + lorry_info['interval']
        lorry_info['due_nice'] = renderer.format_due_nicely(due, now)

        timestamp = time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime(now))

        parts = urllib.parse.urlparse(bottle.request.url)
        host, port = parts.netloc.split(':', 1)
        http_server_root = urllib.parse.urlunparse(
            (parts.scheme, host, '', '', '', ''))

        return bottle.template(
            self._templates['lorry'],
            http_server_root=http_server_root,
            lorry=lorry_info,
            timestamp=timestamp)
