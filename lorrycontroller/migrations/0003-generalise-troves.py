# Copyright (C) 2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import yoyo


yoyo.step('CREATE TABLE hosts ('
          'host TEXT PRIMARY KEY, '
          'protocol TEXT, '
          'username TEXT, '
          'password TEXT, '
          'type TEXT NOT NULL, '
          'type_params TEXT NOT NULL, '
          'lorry_interval INT, '
          'lorry_timeout INT, '
          'ls_interval INT, '
          'ls_last_run INT, '
          'prefixmap TEXT, '
          'ignore TEXT '
          ')')
yoyo.step('INSERT INTO hosts '
          'SELECT trovehost, protocol, username, password, '
          "CASE WHEN gitlab_token IS NULL THEN 'trove' ELSE 'gitlab' END, "
          "CASE WHEN gitlab_token IS NULL THEN '{}' "
          "ELSE '{\"private-token\":\"' || gitlab_token || '\"}' END, "
          'lorry_interval, lorry_timeout, ls_interval, ls_last_run, prefixmap, '
          'ignore '
          'FROM troves')
yoyo.step('DROP TABLE troves')
